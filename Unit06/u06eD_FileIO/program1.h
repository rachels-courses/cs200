#ifndef _PROGRAM_1    // Disallows duplicates
#define _PROGRAM_1    // Disallows duplicates

#include <iostream>   // Using cout and cin
#include <string>     // Using strings
#include <fstream>    // Using file input and output (ifstream, ofstream)
using namespace std;  // Using standard library


// TODO: Add your code to program 1
void Program1()
{
    cout << endl << "------------------------------------------" << endl;
    cout << "TO DO LIST" << endl;

    // Create an output file stream object, load "todo.txt".

    string userInput;
    int counter = 0;

    bool done = false;
    cin.ignore();
    while ( !done )
    {
        counter++;
        cout << "Enter to do item #" << counter << " or STOP to end: ";
        getline( cin, userInput );

        if ( userInput == "STOP" )
        {
            break;
        }

        // Output the counter and userInput to the output file.
    }

    cout << endl << "Saved to todo.txt" << endl << endl;
}



#endif                // Disallows duplicates
