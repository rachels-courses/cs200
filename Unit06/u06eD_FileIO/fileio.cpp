#include <iostream>
using namespace std;

#include "program1.h"
#include "program2.h"
#include "program3.h"
#include "program4.h"
#include "program5.h"

// You don't need to modify main
int main()
{
    ofstream testOutput( "lyrics.txt" );
    testOutput << "Never gonna give you up" << endl
        << "Never gonna let you down" << endl
        << "Never gonna run around and desert you" << endl
        << "Never gonna make you cry" << endl
        << "Never gonna say goodbye" << endl
        << "Never gonna tell a lie and hurt you" << endl;
    testOutput.close();

    bool done = false;
    while ( !done )
    {
        cout << "----------------------------------------" << endl;
        cout << "- MAIN MENU                            -" << endl;
        cout << "----------------------------------------" << endl;
        cout << "- 0. EXIT                              -" << endl;
        cout << "- 1. To do list                        -" << endl;
        cout << "- 2. Voting                            -" << endl;
        cout << "- 3. Reading with >>                   -" << endl;
        cout << "- 4. Reading with getline              -" << endl;
        cout << "- 5. Saving and loading data           -" << endl;
        cout << "----------------------------------------" << endl;
        cout << endl << "SELECTION: ";
        int choice;
        cin >> choice;

        cout << endl << endl;
        cout << "-------------[SELECTED " << choice << "]-------------" << endl;

        switch( choice )
        {
            case 0:     done = true;        break;
            case 1:     Program1();         break;
            case 2:     Program2();         break;
            case 3:     Program3();         break;
            case 4:     Program4();         break;
            case 5:     Program5();         break;
        }

        cout << endl << endl;
    }

    return 0;
}
