#ifndef _PROGRAM_4    // Disallows duplicates
#define _PROGRAM_4    // Disallows duplicates

#include <iostream>   // Using cout and cin
#include <string>     // Using strings
#include <fstream>    // Using file input and output (ifstream, ofstream)
using namespace std;  // Using standard library


void Program4()
{
    cout << endl << "------------------------------------------" << endl;
    cout << "READING WITH GETLINE" << endl;

    string buffer;
    int counter = 0;

    // Create an input file stream object, open "todo.txt".
    // Use a while loop to load in one item at a time with getline
    // Use cout to display each item and the counter values.



    cout << endl << endl;
}


#endif                // Disallows duplicates
