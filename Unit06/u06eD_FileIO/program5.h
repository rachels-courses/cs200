#ifndef _PROGRAM_5    // Disallows duplicates
#define _PROGRAM_5    // Disallows duplicates

#include <iostream>   // Using cout and cin
#include <string>     // Using strings
#include <fstream>    // Using file input and output (ifstream, ofstream)
using namespace std;  // Using standard library


void Program5()
{
    cout << endl << "------------------------------------------" << endl;
    cout << "READ AND WRITE" << endl;

    // Setup with default values
    float bankBalance = 0;
    int accountNumber = rand() % 100000;

    // Create input file stream object here, load "bank.txt"

    // If loading doesn't fail, load in the bankBalance and accountNumber.

    // Close the input file.

    int userInput;

    // Begin program loop
    bool done = false;
    while ( !done )
    {
        cout << endl << "MAIN MENU" << endl;
        cout << "Account #: " << accountNumber << endl;
        cout << "Balance:  $" << bankBalance << endl << endl;
        cout << "1. Deposit" << endl;
        cout << "2. Withdraw" << endl;
        cout << "3. Save and quit" << endl;
        cout << endl << ">> ";

        cin >> userInput;

        // Deposit
        if ( userInput == 1 )
        {
            float amount;
            cout << "Deposit how much? ";
            cin >> amount;

            if ( amount <= 0 )
            {
                cout << "Cannot deposit a negative/zero amount!" << endl;
            }
            else
            {
                bankBalance += amount;
            }
        }
        // Withdraw
        else if ( userInput == 2 )
        {
            float amount;
            cout << "Withdraw how much? ";
            cin >> amount;

            if ( amount <= 0 )
            {
                cout << "Cannot withdraw a negative amount!" << endl;
            }
            else if ( amount > bankBalance )
            {
                cout << "Cannot withdraw more than you have!" << endl;
            }
            else
            {
                bankBalance -= amount;
            }
        }
        // Exit
        else if ( userInput == 3 )
        {
            done = true;
        }
    }

    // Create an input file stream object, open "bank.txt".

    // Output the bankBalance on one line and accountNumber on another.



    cout << endl << endl;
}


#endif                // Disallows duplicates
