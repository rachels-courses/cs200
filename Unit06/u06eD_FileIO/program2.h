#ifndef _PROGRAM_2    // Disallows duplicates
#define _PROGRAM_2    // Disallows duplicates

#include <iostream>   // Using cout and cin
#include <string>     // Using strings
#include <fstream>    // Using file input and output (ifstream, ofstream)
using namespace std;  // Using standard library


void Program2()
{
    cout << endl << "------------------------------------------" << endl;
    cout << "VOTING BOOTH" << endl;

    // Setup voting options
    const int MAX_CHOICES = 5;
    string voteName[MAX_CHOICES]    = { "Samosas", "Tacos", "Onigiri", "Pizza", "QUIT" };
    int voteCount[MAX_CHOICES]      = { 0, 0, 0, 0, 0 };

    int userInput;

    bool done = false;
    while ( !done )
    {
        // Menu
        cout << "OPTIONS:" << endl;
        for ( int i = 0; i < MAX_CHOICES; i++ )
        {
            cout << i << ". " << voteName[i] << endl;
        }

        cout << endl << "Your vote: ";
        cin >> userInput;

        // Validate user input
        while ( userInput < 0 || userInput >= MAX_CHOICES )
        {
            cout << "Invalid selection, try again: ";
            cin >> userInput;
        }

        // Check for quit
        if ( voteName[userInput] == "QUIT" )
        {
            break;
        }

        voteCount[userInput]++;
    }
    
    // Create an output file stream object, open "votes.csv".
    
    // Output the header
    
    // Output the vote results



    cout << "RESULTS SAVED TO votes.csv" << endl << endl;
}


#endif                // Disallows duplicates
