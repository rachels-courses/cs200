#include <iostream>
#include <string>
using namespace std;

#include "Store/Store.h"
#include "Menus/Menus.h"
#include "Utilities/Utilities.h"

int main()
{
    // TODO: Create a Store variable

    bool done = false;
    const int MENU_COUNT = 4;
    string options[MENU_COUNT] = {
        "Quit",
        "Add store item",
        "Edit store item",
        "Display all items"
    };
    while ( !done )
    {
        ClearScreen();
        cout << "--------------- RAINFORESTORE ---------------" << endl;
        cout << "> MAIN MENU" << endl;
        cout << endl;

        for ( int i = 0; i < MENU_COUNT; i++ )
        {
            cout << i << ". " << options[i] << endl;
        }

        cout << ">> ";
        int choice;
        cin >> choice;

        switch( choice )
        {
            case 0:
            done = true;
            break;

            case 1:
                // Add item to store
            break;

            case 2:
                // Edit item in store
            break;

            case 3:
                // Display all store items
            break;

            default:
                cout << "Invalid selection, try again." << endl;
        }
    }

    return 0;
}
