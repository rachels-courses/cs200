#ifndef _UTILITIES_H
#define _UTILITIES_H

#include <string>
#include <sstream>
using namespace std;

void ClearScreen();
void Pause();

template <typename T>
string ToString( const T& value )
{
    stringstream ss;
    ss << value;
    return ss.str();
}

#endif
