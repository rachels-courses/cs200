#ifndef _STOREITEM_H
#define _STOREITEM_H

#include <string>
using namespace std;

struct StoreItem
{
    int index;
    string name;
    float price;

    void Setup( int newIndex, string newName, float newPrice );
    void Display();
};

#endif
