#include "StoreItem.h"

#include <iostream>
#include <iomanip>
using namespace std;

void StoreItem::Setup( int newIndex, string newName, float newPrice )
{
    index = newIndex;
    name = newName;
    price = newPrice;
}

void StoreItem::Display()
{
    cout.precision( 2 );
    cout << left << setw( 5 ) << index
        << setw( 50 ) << name
        << setw( 20 ) << fixed << price << endl;
}
