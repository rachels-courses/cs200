#include "Program2.h"
#include "Die.h"

#include "../Utilities/Utilities.h"

#include <array>
#include <iostream>
using namespace std;

void Program2()
{
    bool done = false;
    while ( !done )
    {
        ClearScreen();
        cout << endl << "--------------- ROLLING DICE ---------------" << endl;

        // TODO: Create one Die object, set its sides

        cout << endl << "Rolling a 6 sided die 10 times ..." << endl;
        for ( int i = 0; i < 10; i++ )
        {
            // TODO: Roll the die, displaying the result.
        }

        cout << endl << endl << "Rolling the bag of dice..." << endl;
        // TODO: Create an array of Die objects, set up each side.

        // TODO: Iterate through all the dice in the array, rolling each one.

        cout << endl;
        char again;
        cout << "Roll again? (y/n): ";
        cin >> again;
        again = tolower( again );

        if ( again == 'n' )
        {
            done = true;
        }
    }
}
