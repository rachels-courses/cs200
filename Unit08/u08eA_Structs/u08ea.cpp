#include <iostream>
using namespace std;

#include "Program1/Program1.h"
#include "Program2/Program2.h"
#include "Program3/Program3.h"
#include "Utilities/Utilities.h"

int main()
{
    bool done = false;
    const int MENU_COUNT = 4;
    string options[MENU_COUNT] = {
        "Quit",
        "Program 1 - Soda Mixer",
        "Program 2 - Rolling Dice",
        "Program 3 - Store"
    };
    while ( !done )
    {
        ClearScreen();

        cout << endl << "--------------- MAIN MENU ---------------" << endl;
        for ( int i = 0; i < MENU_COUNT; i++ )
        {
            cout << i << ". " << options[i] << endl;
        }

        cout << ">> ";
        int choice;
        cin >> choice;

        switch( choice )
        {
            case 0:     done = true;    break;
            case 1:     Program1();     break;
            case 2:     Program2();     break;
            case 3:     Program3();     break;
            default:
                cout << "Invalid selection, try again." << endl;
        }
    }

    return 0;
}
