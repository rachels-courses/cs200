#include "Program1.h"
#include "Soda.h"

#include "../Utilities/Utilities.h"

#include <array>
#include <iostream>
#include <iomanip>
using namespace std;

void Program1()
{
    // TODO: Set up 3 types of soda

    // TODO: Set up one more soda for your mix

    bool done = false;
    while ( !done )
    {
        ClearScreen();
        cout << endl << "--------------- SODA FOUNTAIN ---------------" << endl << endl;
        cout << left
            << setw( 5 ) << "#"
            << setw( 20 ) << "SODA"
            << setw( 20 ) << "OZ"
            << setw( 20 ) << "CALORIES"
            << endl << string( 80, '-' );

        // TODO: Display each of the 3 soda options in a table form

        // TODO: Display a summary of your soda


        cout << endl << "Add which soda to your mix? (or 0 to quit): " << endl;
        int choice;
        cin >> choice;

        switch( choice )
        {
            case 0:
                done = true;
            break;

            case 1:
                // TODO: Add Soda 1 option to your mix
            break;

            case 2:
                // TODO: Add Soda 2 option to your mix
            break;

            case 3:
                // TODO: Add Soda 3 option to your mix
            break;

            default:
                cout << "Invalid option, try again" << endl;
        }
    }
}


