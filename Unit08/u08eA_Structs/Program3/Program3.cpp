#include "Program3.h"
#include "StoreItem.h"

#include "../Utilities/Utilities.h"

#include <array>
#include <iomanip>
#include <iostream>
using namespace std;

void Program3()
{
    cout.precision( 2 );

    const int TOTAL_ITEMS = 4;

    // TODO: Create an array of StoreItems, set up each item for sale using its Setup function.

    float totalPrice = 0;

    bool done = false;
    while ( !done )
    {
        ClearScreen();
        cout << endl << "--------------- RAINFORESTORE ---------------" << endl << endl;
        cout << left
            << setw( 5 ) << "#"
            << setw( 50 ) << "ITEM"
            << setw( 20 ) << "PRICE"
            << endl << string( 80, '-' );

        // TODO: Iterate through all the StoreItems displaying each one with its Display function.

        cout << endl << "Current cart amount: $" << fixed << totalPrice << endl;

        cout << ">> ";
        int choice;
        cin >> choice;

        if ( choice >= 0 && choice < TOTAL_ITEMS )
        {
            // TODO: Add the item's price to the totalPrice variable
        }

        cout << endl;
        char again;
        cout << "Add another item? (y/n): ";
        cin >> again;
        again = tolower( again );

        if ( again == 'n' )
        {
            done = true;
        }
    }

    cout << endl << "Final price: $" << fixed << totalPrice << endl;
    Pause();
}
