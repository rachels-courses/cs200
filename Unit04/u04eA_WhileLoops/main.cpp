#include <iostream>
#include <string>
using namespace std;

#include "program1.h"
#include "program2.h"
#include "program3.h"
#include "program4.h"
#include "program5.h"
#include "program6.h"

// You don't need to modify main here.
int main()
{
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;
        
        cout << endl << endl;
        cout << "------------------------------" << endl;
        
        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }
        else { break; }
        
        cout << endl << endl;
        cout << "------------------------------" << endl;
    }
    
    return 0;
}
